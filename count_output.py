#!/usr/bin/env python
import sys
from itertools import tee
iter1,iter2 = tee(sys.stdin,2)
if all(int(input.split(' ')[0])==-3 for index,input in enumerate(iter1)):
    print 'All output was -3'
else:
    print 'Not all output was -3'
if all(int(input.split(' ')[1]) == index for index,input in enumerate(iter2)):
    print "All indices matched"
else:
    print "Some indices didn't match"
