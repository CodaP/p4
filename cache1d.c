/*
 * Assignment 4: cache1D.c
 * A simple program that inserts the number -3 in
 *      all indexes of a array of size 2,000,000
 *      to simulate how cache works 
 * by Karl Foss section 1
 *    Coda Phillips section 1
 */
#define ARRAY_SIZE 2000000


#ifdef DEBUG_OUTPUT
#    include <stdio.h>
#endif /* DEBUG_OUTPUT */

/* array that holds all the 2000000 -3s */
int array[ARRAY_SIZE];

/* 
 * The main method puts the number -3 into
 *      all 2000000 indexes of the array
 *
 * parameters
 *      argc: the number of arguments
 *      argv: an array of pointers to characters
 *      that points the argument names
 */
int main(int argc, char* argv[])
{
    /* array index  */
    int i;
    /* goes through each index of array adding a "-3"  */
    for(i=0; i<ARRAY_SIZE; i++)
    {
        array[i] = -3;

/* Conditional debug output used to verify that the program works */
#ifdef DEBUG_OUTPUT
        /* Print element and index */
        printf("%d %d\n",array[i],i);
#endif /* DEBUG_OUTPUT */

    }

    return 0;
} /* End of main  */
