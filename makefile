# Assignment #4: makefile
#  by Karl Foss section 1
#     Coda Phillps section 1
CC=gcc
PIN=/p/course/cs354-common/public/cache/pin
CACHE=/p/course/cs354-common/public/cache/source/tools/Memory/obj-ia32/allcache.so
SET=1
CAP=32768
BLOCK=4
EXE=cache1d
ICACHE=$(PIN) -t $(CACHE) -ia $(SET) -is $(CAP) -ib $(BLOCK) -- ./$(EXE)
DCACHE=$(PIN) -t $(CACHE) -da $(SET) -ds $(CAP) -db $(BLOCK) -- ./$(EXE)

all: cache1d cache2Drows cache2Dcols

cache1d: cache1d.c
	$(CC) -Wall -m32 $(CFLAGS) cache1d.c -o cache1d

cache2Drows: cache2Drows.c
	$(CC) -Wall -m32 $(CFLAGS) cache2Drows.c -o cache2Drows

cache2Dcols: cache2Dcols.c
	$(CC) -Wall -m32 $(CFLAGS) cache2Dcols.c -o cache2Dcols

# Simulations are customized by overriding BLOCK and EXE variables
# from the commandline
.PHONY: simulate
simulate: $(EXE)
	$(PIN) -t $(CACHE) -da $(SET) -ia $(SET) -ds $(CAP) -is $(CAP) -db $(BLOCK) -ib $(BLOCK) -- ./$(EXE)

.PHONY: clean
clean:
	rm cache1d
	rm cache2Drows
	rm cache2Dcols

.PHONY: test
test_cache1d: cache1d
	./cache1d | wc -l
	./cache1d | python count_output.py
	
