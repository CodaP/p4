/*
 * Assignment #4: cache2Dcols.c
 * A simple program that inserts the number -3 at
 *      all indexes of a array of size 2,000,000
 *      to test cache properties
 * by Karl Foss section 1
 *    Coda Phillips section 1
 */
#define ROW_SIZE 2000
#define COLUMN_SIZE 1000

/* Global array that will contain all -3 */
int array[ROW_SIZE][COLUMN_SIZE];


/*
 *  The main method puts the number -3 into
 *    all 2000000 indexes of the 2D arrary
 *   
 *  parameters
 *      argc: the number of arguments
 *      argv: an array of pointers to characters
 *  that points the argument names
 */
int main(int argc, char* argv[])
{
    
    /* Row index */
    int i;

    /* Column index */
    int j;

    /* Iterate through all rows */
    for (i = 0; i < COLUMN_SIZE; i++)
    {
        /* Iterate down all elements columns */
        for (j = 0; j < ROW_SIZE; j++)
        {
            array[j][i] = -3;
        }
    }
    return 0;
}/* End of main */

