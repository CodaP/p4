/*
 *  Assignment #4: cache2Drows.c
 *      A simple program that adds the number -3 to
 *      all indexes of a 2D array of size 2000000 that's 2000 by 1000
 *      to simulate how cache works
 *
 *  by Karl Foss section 1
 *  Coda Phillips section 1
 */
#define ROW_SIZE 2000
#define COLUMN_SIZE 1000

/* 2D array that's 2000 by 1000
 *  that will be filled with the number "-3" */
int array[ROW_SIZE][COLUMN_SIZE];


/*
 * The main method puts the number -3 into
 *    all 2000000 indexes of 2000 by 1000
 *    2D array 
 *    
 *  parameters
 *      argc: the number of arguments
 *      argv: an array of pointers to characters
 *      that points the argument names
 */
int main(int argc, char* argv[])
{
    /* The Column index */
    int i;
    /* The Row index */
    int j;

    /* Iterate through each column */
    for (i = 0; i < ROW_SIZE; i++)
    {
        /* Iterate through all row elements */
        for (j = 0; j < COLUMN_SIZE; j++)
        {
            array[i][j] = -3;
        }
    }
    return 0;
}/* End of main */

